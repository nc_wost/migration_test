﻿using BaseCulture.Database;
using BaseCulture.Models;

namespace MigrationTest.Database
{
    public class DbInitializer
    {
        public static void Initialize(DatabaseContext context, ISeeder seeder)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            seeder.Seed(context);
        }
    }
}
