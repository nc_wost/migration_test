﻿using BaseCulture.Database;
using BaseCulture.Models;
using Microsoft.EntityFrameworkCore;

namespace MigrationTest.Database
{
    public class DatabaseContext : DbContext
    {
        private IModelCreator _modelCreator;
        public DatabaseContext(DbContextOptions<DatabaseContext> options,
            IModelCreator modelCreator) : base(options)
        {
            _modelCreator = modelCreator;
        }

        public DbSet<BaseOrganisation> Organisations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _modelCreator.CreateModel(modelBuilder);

        }
    }
}
