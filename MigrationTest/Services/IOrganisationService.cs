﻿using System.Collections.Generic;

namespace MigrationTest.Services
{
    public interface IOrganisationService
    {
        object GetById(string id);
        IEnumerable<object> GetOrganisations();
    }
}
