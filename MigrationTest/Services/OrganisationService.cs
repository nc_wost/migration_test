﻿using BaseCulture.Dtos;
using MigrationTest.Database;
using System.Collections.Generic;
using System.Linq;

namespace MigrationTest.Services
{
    public class OrganisationService : IOrganisationService
    {
        private DatabaseContext _databaseContext;
        private IDtoMapper _dtoMapper;

        public OrganisationService(DatabaseContext databaseContext, IDtoMapper dtoMapper)
        {
            _databaseContext = databaseContext;
            _dtoMapper = dtoMapper;
        }

        public object GetById(string id)
        {
            var organisation = _databaseContext.Organisations.SingleOrDefault(x => x.Id == id);
            var organisationDto = _dtoMapper.MapOrganisationToDto(organisation);
            return organisationDto;
        }

        public IEnumerable<object> GetOrganisations()
        {
            var organisations = _databaseContext.Organisations.ToList();
            var organisationDtos = new List<object>();
            foreach (var organisation in organisations)
            {
                organisationDtos.Add(_dtoMapper.MapOrganisationToDto(organisation));
            }
            return organisationDtos;
        }
    }
}
