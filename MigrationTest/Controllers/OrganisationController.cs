﻿using Microsoft.AspNetCore.Mvc;
using MigrationTest.Services;

namespace MigrationTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrganisationController : ControllerBase
    {
        private readonly IOrganisationService _organisationService;

        public OrganisationController(IOrganisationService organisationService)
        {
            _organisationService = organisationService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var organisations = _organisationService.GetOrganisations();
            return Ok(organisations);
        }
    }
}
