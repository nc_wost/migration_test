﻿using BaseCulture.Database;
using Microsoft.EntityFrameworkCore;
using NorwegianCulture.Models;

namespace NorwegianCulture.Database
{
    public class ModelCreator : IModelCreator
    {
        public void CreateModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organisation>().ToTable("Organisations");
        }
    }
}
