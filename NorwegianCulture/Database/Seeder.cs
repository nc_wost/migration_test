﻿using BaseCulture.Database;
using BaseCulture.Models;
using Microsoft.EntityFrameworkCore;
using NorwegianCulture.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorwegianCulture.Database
{
    public class Seeder : ISeeder
    {
        public void Seed(DbContext dbContext)
        {
            var organisationSeeds = GetOrganisationSeeds();
            dbContext.Set<BaseOrganisation>().AddRange(organisationSeeds);
            dbContext.SaveChanges();
        }

        private IEnumerable<BaseOrganisation> GetOrganisationSeeds()
        {
            return new List<BaseOrganisation>
            {
                new Organisation
                {
                    Id = "Nip123",
                    Name = "Orlen",
                    NorwegianSpecific = "Something Norwegian",
                },
            };
        }
    }
}
