﻿using BaseCulture.Dtos;
using BaseCulture.Models;
using NorwegianCulture.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorwegianCulture.Dtos
{
    public class DtoMapper : IDtoMapper
    {
        public BaseOrganisationDto MapOrganisationToDto(BaseOrganisation organisation)
        {
            var norwegianOrganisation = organisation as Organisation;
            return new OrganisationDto
            {
                Id = norwegianOrganisation.Id,
                Name = norwegianOrganisation.Name,
                NorwegianSpecific = norwegianOrganisation.NorwegianSpecific,
            };
        }
    }
}
