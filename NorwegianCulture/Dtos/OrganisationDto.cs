﻿using BaseCulture.Dtos;

namespace NorwegianCulture.Dtos
{
    public class OrganisationDto : BaseOrganisationDto
    {
        public string NorwegianSpecific { get; set; }
    }
}
