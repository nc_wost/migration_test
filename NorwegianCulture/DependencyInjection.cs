﻿using BaseCulture.Database;
using BaseCulture.Dtos;
using Microsoft.Extensions.DependencyInjection;
using NorwegianCulture.Database;
using NorwegianCulture.Dtos;

namespace NorwegianCulture
{
    public static class DependencyInjection
    {
        public static void ConfigureForNorway(this IServiceCollection services)
        {
            services.AddSingleton<IModelCreator, ModelCreator>();
            services.AddSingleton<IDtoMapper, DtoMapper>();
            services.AddSingleton<ISeeder, Seeder>();
        }
    }
}
