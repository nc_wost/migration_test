﻿using BaseCulture.Models;

namespace NorwegianCulture.Models
{
    public class Organisation : BaseOrganisation
    {
        public string NorwegianSpecific { get; set; }
    }
}
