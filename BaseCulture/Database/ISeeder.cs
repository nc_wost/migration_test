﻿using BaseCulture.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace BaseCulture.Database
{
    public interface ISeeder
    {
        void Seed(DbContext dbContext);
    }
}
