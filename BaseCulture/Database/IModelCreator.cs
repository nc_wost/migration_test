﻿using Microsoft.EntityFrameworkCore;

namespace BaseCulture.Database
{
    public interface IModelCreator
    {
        void CreateModel(ModelBuilder modelBuilder);
    }
}
