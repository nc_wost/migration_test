﻿using BaseCulture.Models;

namespace BaseCulture.Dtos
{
    public interface IDtoMapper
    {
        BaseOrganisationDto MapOrganisationToDto(BaseOrganisation organisation);
    }
}
