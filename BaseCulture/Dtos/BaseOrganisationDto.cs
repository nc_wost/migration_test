﻿namespace BaseCulture.Dtos
{
    public abstract class BaseOrganisationDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}