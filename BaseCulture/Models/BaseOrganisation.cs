﻿namespace BaseCulture.Models
{
    public abstract class BaseOrganisation
    {
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }
    }
}
