﻿using BaseCulture.Models;

namespace PolishCulture.Models
{
    public class Organisation : BaseOrganisation
    {
        public string Krs { get; set; }
        public string Regon { get; set; }
    }
}
