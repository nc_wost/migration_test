﻿using BaseCulture.Database;
using Microsoft.EntityFrameworkCore;
using PolishCulture.Models;

namespace PolishCulture.Database
{
    public class ModelCreator : IModelCreator
    {
        public void CreateModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organisation>().ToTable("Organisations");
        }
    }
}
