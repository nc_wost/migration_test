﻿using BaseCulture.Database;
using BaseCulture.Models;
using Microsoft.EntityFrameworkCore;
using PolishCulture.Models;
using System.Collections.Generic;

namespace PolishCulture.Database
{
    public class Seeder : ISeeder
    {
        public void Seed(DbContext dbContext)
        {
            var organisationSeeds = GetOrganisationSeeds();
            dbContext.Set<BaseOrganisation>().AddRange(organisationSeeds);
            dbContext.SaveChanges();
        }

        private IEnumerable<BaseOrganisation> GetOrganisationSeeds()
        {
            return new List<BaseOrganisation>
            {
                new Organisation
                {
                    Id = "Nip123",
                    Name = "Orlen",
                    Krs = "Krs123",
                    Regon = "Regon123",
                },
            };
        }
    }
}
