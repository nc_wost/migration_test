﻿using BaseCulture.Database;
using BaseCulture.Dtos;
using Microsoft.Extensions.DependencyInjection;
using PolishCulture.Database;
using PolishCulture.Dtos;

namespace PolishCulture
{
    public static class DependencyInjection
    {
        public static void ConfigureForPoland(this IServiceCollection services)
        {
            services.AddSingleton<IModelCreator, ModelCreator>();
            services.AddSingleton<IDtoMapper, DtoMapper>();
            services.AddSingleton<ISeeder, Seeder>();
        }
    }
}
