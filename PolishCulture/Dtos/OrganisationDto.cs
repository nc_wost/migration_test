﻿using BaseCulture.Dtos;

namespace PolishCulture.Dtos
{
    public class OrganisationDto : BaseOrganisationDto
    {
        public string Krs { get; set; }
        public string Regon { get; set; }
    }
}
