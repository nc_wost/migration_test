﻿using BaseCulture.Dtos;
using BaseCulture.Models;
using PolishCulture.Models;

namespace PolishCulture.Dtos
{
    public class DtoMapper : IDtoMapper
    {
        public BaseOrganisationDto MapOrganisationToDto(BaseOrganisation organisation)
        {
            var polishOrganisation = organisation as Organisation;
            return new OrganisationDto
            {
                Id = polishOrganisation.Id,
                Name = polishOrganisation.Name,
                Krs = polishOrganisation.Krs,
                Regon = polishOrganisation.Regon,
            };
        }
    }
}
