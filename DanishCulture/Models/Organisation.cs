﻿using BaseCulture.Models;

namespace DanishCulture.Models
{
    public class Organisation : BaseOrganisation
    {
        public string ProductionNumber { get; set; }
        public bool IsProductionUnit { get; set; }
    }
}
