﻿using BaseCulture.Database;
using BaseCulture.Dtos;
using DanishCulture.Database;
using DanishCulture.Dtos;
using Microsoft.Extensions.DependencyInjection;

namespace DanishCulture
{
    public static class DependencyInjection
    {
        public static void ConfigureForDenmark(this IServiceCollection services)
        {
            services.AddSingleton<IModelCreator, ModelCreator>();
            services.AddSingleton<IDtoMapper, DtoMapper>();
            services.AddSingleton<ISeeder, Seeder>();
        }
    }
}
