﻿using BaseCulture.Database;
using DanishCulture.Models;
using Microsoft.EntityFrameworkCore;

namespace DanishCulture.Database
{
    public class ModelCreator : IModelCreator
    {
        public void CreateModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organisation>().ToTable("Organisations");
        }
    }
}
