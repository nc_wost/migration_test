﻿using BaseCulture.Database;
using BaseCulture.Models;
using DanishCulture.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DanishCulture.Database
{
    public class Seeder : ISeeder
    {
        public void Seed(DbContext dbContext)
        {
            var organisationSeeds = GetOrganisationSeeds();
            dbContext.Set<BaseOrganisation>().AddRange(organisationSeeds);
            dbContext.SaveChanges();
        }

        private IEnumerable<BaseOrganisation> GetOrganisationSeeds()
        {
            return new List<BaseOrganisation>
            {
                new Organisation
                {
                    Id = "cvr123",
                    Name = "JYSK",
                    ProductionNumber = "ProdNumber123",
                    IsProductionUnit = true,
                },
            };
        }
    }
}
