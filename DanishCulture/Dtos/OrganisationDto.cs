﻿using BaseCulture.Dtos;

namespace DanishCulture.Dtos
{
    public class OrganisationDto : BaseOrganisationDto
    {
        public string ProductionNumber { get; set; }
        public bool IsProductionUnit { get; set; }
    }
}
