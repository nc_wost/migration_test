﻿using BaseCulture.Dtos;
using BaseCulture.Models;
using DanishCulture.Models;

namespace DanishCulture.Dtos
{
    public class DtoMapper : IDtoMapper
    {
        public BaseOrganisationDto MapOrganisationToDto(BaseOrganisation organisation)
        {
            var danishOrganisation = organisation as Organisation;
            return new OrganisationDto
            {
                Id = danishOrganisation.Id,
                Name = danishOrganisation.Name,
                ProductionNumber = danishOrganisation.ProductionNumber,
                IsProductionUnit = danishOrganisation.IsProductionUnit,
            };
        }
    }
}
